import React, {Component} from 'react';
import Index from './Screen/index';
import About from './Screen/AboutScreen'
import {Provider} from 'react-redux'
import store from './src/_redux/store'
import * as Font from 'expo-font';


export default class App extends Component {

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }



  render(){
  return (
    <Provider store={store}>
    <Index />
    </Provider>
  );
}
}
