const initialState = {
    data: [],
    dataErr: [],
    isLogin: false,
    isLoading: false
  };
  
  const LoginR = (state = initialState, action) => {
    switch (action.type) {
      case `LOGIN_PENDING`:
        return {
          ...state
        };
      case `LOGIN_FULFILLED`:
        return {
          ...state,
          data: action.payload.data,
          isLoading: false,
          isLogin: true,
          passErr: action.payload.data.passErr,
          userErr:action.payload.data.userErr,
          statLogin: action.payload.data.statLogin
        };
      case `LOGIN_REJECTED`:
        return {
          ...state,
          isLoading: false,
          error: true,
          dataErr: action.payload.data
        };
      default:
        return state;
    }
  };
  
  export default LoginR;