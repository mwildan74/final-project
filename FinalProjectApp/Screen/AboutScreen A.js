
import React, { Component } from 'react'
import { StyleSheet, Text, Image, View, TouchableOpacity, Linking } from 'react-native'

import { robotoWeights,material,human } from 'react-native-typography'

export default class AboutA extends Component {
    


    render() {
        return (
            <>
            <View style={{backgroundColor: 'white', }}>
            <View style={{backgroundColor: 'white', justifyContent:'center', alignItems:"center"}}>
            <Image source={require('./assets/aboutprofile.jpg')} style={styles.profile} />
            <Text style={human.headline} style={{marginTop:10, fontSize:20}}>Daffa Abdhy Muzhaffar</Text>
            </View>
            <View style={{alignItems:"center", justifyContent:"center"}}>
            <View style={{ backgroundColor:'white', height:100, width:"90%", borderWidth:1,marginTop:20, borderColor:'#fd0759', shadowColor: "#000",
            shadowOffset: {
	          width: 0,
	          height: 7,
            },
            shadowOpacity: 0.41,
            shadowRadius: 9.11,

            elevation: 14, borderRadius:12}}>
                <Text style={robotoWeights.bold} style={{marginLeft:5,marginTop:5}}>Hi, My Name is Daffa, I'm an Fullstack Developer that currently
                focus on developing Front end using
                React JS & React Native for mobile
                application, and now I'm working at PT. Asia Sekuriti Indonesia as a Front End Developer :)</Text>
            </View>
            </View>
            <Text style={{marginTop:50, fontWeight:'bold', marginLeft:10}}>Connect with me</Text>



            <View style={{ flexDirection:'row', justifyContent:"space-around", marginTop:40, backgroundColor: 'white'}}>
            <TouchableOpacity style={{width:100,height:100}} onPress={() =>
                Linking.openURL(
                  'http://bit.ly/daffa-m',
                )
              }>
            <Image source={require('./assets/Linkedinicon.png')}/>
            </TouchableOpacity>

            <TouchableOpacity style={{width:100,height:100}} onPress={() =>
                Linking.openURL(
                  'http://instagram.com/daffaam18'
                )
              }>
            <Image source={require('./assets/instagram.png')}/>
            </TouchableOpacity>

            <TouchableOpacity style={{width:100,height:100}} onPress={() => Linking.openURL('http://github.com/DaffaAM')}>
            <Image source={require('./assets/githubicon.png')} />
            </TouchableOpacity>

            </View>
            <View style={{flexDirection:'row', justifyContent:'space-around'}}>
            <TouchableOpacity style={{width:100,height:100}} onPress={() => Linking.openURL('http://facebook.com/thejail009')}>
            <Image source={require('./assets/facebookicon.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{width:100,height:100}} onPress={() => Linking.openURL('https://api.whatsapp.com/send?phone=+6281210313944&text=Halo%20gan%20mau%20tanya?')}>
            <Image source={require('./assets/waicon.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{width:100,height:100}}>
            <Image source={require('./assets/twitter.png')} />
            </TouchableOpacity>
            </View>
            <View style={{height:200,backgroundColor: 'white'}}>

            </View>
            </View>
            </>
        )
    }
}


const styles = StyleSheet.create({
profile: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    overflow: "hidden",
    borderColor: "black",
    marginTop:70
}


})