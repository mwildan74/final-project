import React from "react";
import {AsyncStorage} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';


import Login from './LoginScreen';
import Home from './HomeScreen';
import About from './AboutScreen'
import AboutA from "./AboutScreen A";
import Detail from "./DetailScreen"
import Test from './TestScreen'
import Restaurant from './Restaurant'

export default () => {


    

    const Tabs = createBottomTabNavigator();
    const TabScreen = () => {
        return (
            <Tabs.Navigator>
                <Tabs.Screen name="Muhammad Wildan" component={About} />
                <Tabs.Screen name="Daffa Abdhy Muzhaffar" component={AboutA} />
            </Tabs.Navigator>
        )
    }

    const token = AsyncStorage.getItem("token")
    const removeItemValue = async (token) => {
        try {
            await AsyncStorage.removeItem(token);
            return true;
        }
        catch(exception) {
            return false;
        }
    }

    const Drawer = createDrawerNavigator();
    const DrawerScreen = () => {
        return (
            <Drawer.Navigator>
                <Drawer.Screen name="Home" component={Home} />
                <Drawer.Screen name="About" component={TabScreen} />
                <Drawer.Screen name="Logout" onPress={removeItemValue} component={Login}/>
                <Drawer.Screen name="Test" component={Test} />
            </Drawer.Navigator>
        )
    }

    const Stack = createStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="Drawer" component={DrawerScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Detail" component={Detail} options={{ headerShown: false }} />
                <Stack.Screen name="Restaurant" component={Restaurant} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}