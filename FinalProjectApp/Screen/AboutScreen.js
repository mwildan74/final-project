import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

export default class About extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ fontWeight: 'bold', fontSize: 24, textAlign: 'center', }}>About Development</Text>
                <View style={styles.item}>
                    <FontAwesome5 name="user-alt" size={200} color="black" />
                </View>
                <View style={styles.text}>
                    <Text style={styles.judul}>Nama</Text>
                    <Text style={styles.isi}>Muhammad Wildan</Text>
                    <Text style={styles.judul}>Social Media</Text>
                    <View style={styles.sosmed}>
                        <AntDesign name="instagram" size={50} color="black" />
                        <AntDesign name="twitter" size={50} color="black" />
                        <AntDesign name="facebook-square" size={50} color="black" />
                    </View>
                    <Text style={styles.judul}>Portofolio</Text>
                    <View style={styles.portofolio}>
                        <AntDesign name="gitlab" size={50} color="black" />
                        <AntDesign name="linkedin-square" size={50} color="black" />

                    </View>

                </View>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 15,
        marginTop:25,

    },
    buttonBack: {
        height: 50,
        width: 50,
        backgroundColor: 'white',
        marginTop: 23,
        marginLeft: 20,
        marginBottom: 20
    },
    item: {
        alignItems: 'center'
    },
    text: {
        height: 350,
        justifyContent: 'space-evenly',
    },
    judul: {
        fontWeight: 'bold',
        fontSize: 24
    },
    isi: {
        paddingLeft: 15,
        fontSize: 20
    },
    sosmed: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    portofolio: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    }
})