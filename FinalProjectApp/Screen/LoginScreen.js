import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, AsyncStorage } from 'react-native';
import {connect} from 'react-redux'
import {LoginA} from '../src/_action/LoginA'
import {CheckUserA} from '../src/_action/CheckUserA'

class App extends Component {
    constructor(){
    super();
    this.state = {
    user:'',
    pass:'',
    userErr: '',
    passErr: '',
    }
    }


    componentDidMount(){
    this.props.CheckUserA();
    const token = AsyncStorage.getItem("token");
    console.log(token)
    }



    handleLogin = () =>{
    const data ={
    user:this.state.user,
    pass:this.state.pass    
        }
    this.props.LoginA(data)
    if( data.statLogin === 'true'){
    AsyncStorage.setItem("token", data.token)
    this.props.navigation.navigate('Drawer')
     } else{
         console.log("error")
     }
    }


    
render(){

    const {data, userErr, passErr }= this.props.LoginR

    if (data.length !== 0) {
        if (data.statLogin === true) {
          AsyncStorage.setItem("token", data.token);
            this.props.navigation.navigate('Drawer')
        } else if (data.statUser === false) inValidUser = true;
        else if (data.statPass === false) inValidPass = true;
      }

    return (
        <View style={styles.container}>
            <View style={styles.item}>
                <Image source={require('./assets/iconLogin.png')} style={styles.Image} />
                <Text style={styles.textlog}>Please Login using your account to continue</Text>
                <View style={styles.wrapTextInput}>
                    <TextInput style={styles.textInput} placeholder={'Username'} onChangeText={(user) => this.setState({user})}
                    value={this.state.username} ></TextInput>
                    <Text style={styles.error}>{userErr}</Text>
                    <TextInput secureTextEntry style={styles.textInput2} placeholder={'Password'} onChangeText={(pass) => this.setState({pass})}
                    value={this.state.pass}></TextInput>
                    <Text style={styles.error}>{passErr}</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>

    );  
    }
}

const mapStateToProps = (state) => {
    return {
      LoginR: state.LoginR,
      CheckUserR: state.CheckUserR,

    };
  };
  const mapDispatchToProps = (dispatch) => {
    return {
      LoginA: (data) => dispatch(LoginA(data)),
      CheckUserA: () => dispatch(CheckUserA())
    };
  };
  

export default connect(mapStateToProps,mapDispatchToProps)(App);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }, item: {
        alignItems: 'center',
        marginTop: 10
    },
    Image: {
        height: 200,
        width: 340,
        backgroundColor: 'black',
        marginTop:100
    },
    textlog: {
        marginTop: 50,
        fontSize: 17,
        fontWeight: 'bold'
    },
    textInput: {
        borderColor: 'black',
        borderWidth: 1,
        height: 52,
        width: 300,
        paddingLeft: 10,
        borderRadius: 50

    },
    textInput2: {
        borderColor: 'black',
        borderWidth: 1,
        height: 52,
        width: 300,
        paddingLeft: 10,
        borderRadius: 50,
       
    },
    wrapTextInput: {
        height: 190,
        marginBottom: 50,
        marginTop: 20,
        justifyContent: 'space-between',

    },
    button: {
        height: 42,
        width: 160,
        backgroundColor: '#00E6E6',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50

    },
    error:{
    color:'red'
    },
    success:{
    color:'green'
    }
})