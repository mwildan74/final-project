import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

export default class Detail extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.buttonBack} onPress={() => this.props.navigation.navigate('Home')} >
                    <FontAwesome5 name="arrow-circle-left" size={30} color="black" />
                </TouchableOpacity >
                <View style={styles.item}>
                    <FontAwesome name="picture-o" size={250} color="black" />
                </View>
                <View style={styles.text}>
                    <Text style={styles.judul}>Nama Item</Text>
                    <Text style={styles.isi}>Example Item</Text>
                    <Text style={styles.judul}>Jenis Item</Text>
                    <Text style={styles.isi}>Example jenis item</Text>
                    <Text style={styles.judul}>Deskripsi</Text>
                    <Text style={styles.isi}>Example Example Example Example ExampleExample Example</Text>
                </View>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 10

    },
    buttonBack: {
        height: 50,
        width: 50,
        backgroundColor: 'white',
        marginTop: 23,
        marginLeft: 20,
        marginBottom: 20
    },
    item: {
        alignItems: 'center'
    },
    text: {
        height: 280,
        justifyContent: 'space-evenly',
    },
    judul: {
        fontWeight: 'bold',
        fontSize: 24
    },
    isi: {
        paddingLeft: 15
    }
})